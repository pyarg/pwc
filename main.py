import ctypes, os, sys


def main():

    # Level 1
    if not is_admin_level():
        return

    # Level 2
    if not args_level():
        return

    # Level 3
    if not pass_args():
        return


def is_admin_level():
    """
    Level 1
    This level require from the user to run the program as administrator
    Helpful commands: sudo
    """

    try:
        admin = os.getuid() == 0
    except AttributeError:
        admin = ctypes.windll.shell32.IsUserAnAdmin() != 0

    if not admin:
        print("Administrator is required")
        return False
    else:
        print("Level up!")
        return True


def args_level():
    """
    Level 2
    This level shows the user how to create Environment Variable
    Helpful commands: win - setx, UNIX - export
    """

    ENV = "TEST_VAR"
    VAL = "hello"

    if os.getenv(ENV) == VAL:
        print("Level up!")
        return True
    else:
        print("No Environment Variable named \"{0}\" and equal to \"{1}\" found".format(ENV, VAL))
        return False


def pass_args():
    pass


if __name__ == '__main__':
    main()
